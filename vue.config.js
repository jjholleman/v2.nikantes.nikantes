module.exports = {
    "publicPath": "",
    "transpileDependencies": [
        "vuetify"
    ],
    css: {
        loaderOptions: {
            scss: {
                prependData: `
                @import "@/assets/scss/_variables.scss";
                @import "@/assets/scss/_mixins.scss";
                `
            }
        }
    },
    pwa: {
        iconPaths: {
            favicon16: '/images/favicon.ico',
            favicon32: '/images/favicon.ico',
            maskIcon: '/images/logo-nikantes.png',
            msTileImage: '/images/logo-nikantes.png',
            appleTouchIcon: '/images/logo-nikantes.png',
        },
        themeColor: '#000000',
        manifestOptions: {
            name: "Sportvereniging Nikantes",
            short_name: "Nikantes",
            theme_color: "#000000",
            background_color: "#ffffff",
            start_url: "/",
            display: "standalone",
            icons: [
                {
                    src: "/images/favicon.ico",
                    sizes: "64x64 32x32 24x24 16x16",
                    type: "image/x-icon"
                },
                {
                    src: "/images/logo-nikantes.png",
                    sizes: "192x192 512x512",
                    type: "image/png"
                }
            ],
        },
    }
};
