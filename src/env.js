const DEBUG = false;
let SITELINK = "https://nikantes.nl";
if(DEBUG) {
    SITELINK ="http://nikantes.nikantes.jordi.oo";
}

const WP_API = SITELINK + "/wp-json/wp/v2";
const ACF_API = SITELINK + "/wp-json/acf/v3";
const MENU_API = SITELINK + "/wp-json/menus/v1";

export default {
    SITELINK: SITELINK,
    WP_API: WP_API,
    ACF_API: ACF_API,
    MENU_API: MENU_API,
}
