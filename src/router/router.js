import Vue from 'vue'
import Router from 'vue-router'
import Home from "../components/Home";
import _id from "../pages/_id";

Vue.use(Router);

const meta = {
    defaults: {
        title: 'Nikantes',
        description: 'Korfbal | Beachkorfbal | Jeu de boules | Sportvereniging Nikantes in Hoogvliet Rotterdam | Sportplus vereniging | Voor jong en oud'
    }
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '',
            name: 'Home',
            component: Home,
            meta: meta.defaults
        },
        {
            path: '/',
            redirect: Home
        },
        {
            path: '/:id',
            name: 'posts',
            component: _id,
            meta: meta.defaults
        }
    ]
})
